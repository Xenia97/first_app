﻿using System;

namespace MyFirstLib
{
    public  class MessageGetter
    {
        private string _message;
        public string Message 
        {
            get { return "Ваше сообщение: " + _message; }
            set { _message = value.Replace("бабы - дуры", "<censored>").Replace("men are goats", "<censored, but true>"); } 
        }
        public MessageGetter(string messageToSet)
        {
            Message = messageToSet;
        }

 
    }
}
